// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/PlayerWeaponComponent.h"
#include "Interface/InstsnceInterface.h"
#include "Interface/InterracWithCharInterface.h"
#include "Components/ArrowComponent.h"
#include "Items/Weapon/MaineWeapon.h"
#include "Items/Weapon/WeaponType1.h"


void UPlayerWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	SelectedWeaponId = TEXT("EMPTY");

	TMap< FName, FWeaponData> AllWeaponVal;
	FName SelectedWeaponIdVal;
	bool ShowAmmoPanelVal;
	bool instanceDataVal;

	IInstsnceInterface::Execute_LoadWeaponSelectInfo(GetWorld()->GetGameInstance(), AllWeaponVal, SelectedWeaponIdVal, ShowAmmoPanelVal, instanceDataVal);

	if (instanceDataVal)
	{
		AllWeapon = AllWeaponVal;
		SelectedWeaponId = SelectedWeaponIdVal;
		ShowAmmoPanel = ShowAmmoPanelVal;

		{
			TArray< FName> Load1;
			IInstsnceInterface::Execute_LoadWeaponEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot1, Load1);
			WeaponCanEquip[EWeaponSlots::Slot1] = Load1;
			TArray< FName> Load2;
			IInstsnceInterface::Execute_LoadWeaponEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot2, Load2);
			WeaponCanEquip[EWeaponSlots::Slot2] = Load2;
			TArray< FName> Load3;
			IInstsnceInterface::Execute_LoadWeaponEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot3, Load3);
			WeaponCanEquip[EWeaponSlots::Slot3] = Load3;
			TArray< FName> Load4;
			IInstsnceInterface::Execute_LoadWeaponEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot4, Load4);
			WeaponCanEquip[EWeaponSlots::Slot4] = Load4;
			TArray< FName> Load5;
			IInstsnceInterface::Execute_LoadWeaponEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot5, Load5);
			WeaponCanEquip[EWeaponSlots::Slot5] = Load5;
		}

		if (SelectedWeaponId != TEXT("EMPTY") ||  !(SelectedWeaponId == ""))
		{

			FWeaponData* SelectedWeaponData = AllWeapon.Find(SelectedWeaponId);

			//��� ������� ��������� ������ � �����������
			IInterracWithCharInterface::Execute_ShowCross(GetOwner(), false);

			if (SelectedWeaponData->WeaponClass)
			{
				FVector point;
				SelectedWeapon = Cast<AMaineWeapon>(GetWorld()->SpawnActor(SelectedWeaponData->WeaponClass, &point));
				ShowAmmoPanel = true;
				UArrowComponent* AttachComponent = nullptr;
				IInterracWithCharInterface::Execute_GetArrowComponent(GetOwner(), AttachComponent);

				SelectedWeapon->SetOwner(GetOwner());
				SelectedWeapon->AttachToComponent(AttachComponent, FAttachmentTransformRules::SnapToTargetIncludingScale, AttachComponent->GetAttachSocketName());
				IInterracWithCharInterface::Execute_ShowCross(GetOwner(), true);

			}
			IInterracWithCharInterface::Execute_UpdateAmmoWidget(GetOwner(), ShowAmmoPanel, SelectedWeaponData->CageCountAmmo, SelectedWeaponData->AmmoCount);
		}

	}
	
}

void UPlayerWeaponComponent::SaveDataToInstance()
{
	
	IInstsnceInterface::Execute_ClearInstanceWeaponData(GetWorld()->GetGameInstance());
	IInstsnceInterface::Execute_SaveWeaponSelectInfo(GetWorld()->GetGameInstance(), AllWeapon, SelectedWeaponId, ShowAmmoPanel, true);

	IInstsnceInterface::Execute_SaveWeaponCanEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot1, WeaponCanEquip[EWeaponSlots::Slot1]);
	IInstsnceInterface::Execute_SaveWeaponCanEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot2, WeaponCanEquip[EWeaponSlots::Slot2]);
	IInstsnceInterface::Execute_SaveWeaponCanEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot3, WeaponCanEquip[EWeaponSlots::Slot3]);
	IInstsnceInterface::Execute_SaveWeaponCanEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot4, WeaponCanEquip[EWeaponSlots::Slot4]);
	IInstsnceInterface::Execute_SaveWeaponCanEquip(GetWorld()->GetGameInstance(), EWeaponSlots::Slot5, WeaponCanEquip[EWeaponSlots::Slot5]);
	
}
