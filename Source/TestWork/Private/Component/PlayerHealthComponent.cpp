// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/PlayerHealthComponent.h"
#include "Interface/InstsnceInterface.h"
#include "Interface/InterracWithCharInterface.h"
#include "Interface/CombatInterface.h"

void UPlayerHealthComponent::LoadDataFromInstance()
{
	int32 LoadHealth;
	bool flag = false;
	IInstsnceInterface::Execute_LoadHealthData(GetWorld()->GetGameInstance(), LoadHealth, flag);

	if (flag)
	{
		Health = LoadHealth;

		if (Health <= 0)
		{
			Health = 0;
			death = true;
			ICombatInterface::Execute_DeathEvent(GetOwner());
		}

		IInterracWithCharInterface::Execute_UpdateHealth(GetOwner(), Health);
	}
}

void UPlayerHealthComponent::SaveDataToInstance()
{
	if (Health > 0)
	{
		IInstsnceInterface::Execute_SaveHealthData(GetWorld()->GetGameInstance(), Health, true);
	}
}
