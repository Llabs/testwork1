// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/HealthComponent.h"
#include "Interface/InterracWithCharInterface.h"
#include "Interface/CombatInterface.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::ChangeHealth(int32 value)
{
	if (!death)
	{
		Health += value;

		if (Health > 100)
		{
			Health = 100;
		}

		if (Health <= 0)
		{
			Health = 0;
			death = true;
			ICombatInterface::Execute_DeathEvent(GetOwner());
		}

		IInterracWithCharInterface::Execute_UpdateHealth(GetOwner(), Health);
	}
}

int32 UHealthComponent::GetHealth()
{
	return Health;
}

