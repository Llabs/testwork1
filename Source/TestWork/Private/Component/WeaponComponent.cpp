// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/WeaponComponent.h"
#include "Interface/InterracWithCharInterface.h"
#include "Components/ArrowComponent.h"
#include "Items/Weapon/MaineWeapon.h"
#include "Items/Weapon/WeaponType1.h"



// Sets default values for this component's properties
UWeaponComponent::UWeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	TArray< FName> list1;
	TArray< FName> list2;
	TArray< FName> list3;
	TArray< FName> list4;
	TArray< FName> list5;

	WeaponCanEquip.Add(EWeaponSlots::Slot1, list1);
	WeaponCanEquip.Add(EWeaponSlots::Slot2, list2);
	WeaponCanEquip.Add(EWeaponSlots::Slot3, list3);
	WeaponCanEquip.Add(EWeaponSlots::Slot4, list4);
	WeaponCanEquip.Add(EWeaponSlots::Slot5, list5);

}


// Called every frame
void UWeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponComponent::AddNewWeapon(FName WeaponId)
{
	if (AllWeapon.Contains(WeaponId)) 
	{
		FWeaponData* WeaponData = AllWeapon.Find(WeaponId);
		TArray< FName> *slotPos = WeaponCanEquip.Find(WeaponData->WeaponSlot);
		slotPos->AddUnique(WeaponId);
		slotPos->Sort([](const FName& A, const FName& B) {
			return A < B; });
	}
}

void UWeaponComponent::AddAmmo(FName WeaponId, int32 ammo)
{
	if (AllWeapon.Contains(WeaponId))
	{
		FWeaponData* WeaponData = AllWeapon.Find(WeaponId);
		WeaponData->AmmoCount += ammo;

		if (WeaponData->AmmoCount > WeaponData->MaxAmmo)
		{
			WeaponData->AmmoCount = WeaponData->MaxAmmo;
		}
		IInterracWithCharInterface::Execute_UpdateAmmoWidget(GetOwner(), ShowAmmoPanel, WeaponData->CageCountAmmo, WeaponData->AmmoCount);
	}
}

void UWeaponComponent::SelectWeapon(EWeaponSlots slot)
{
	SelectMode = true;

	if (slot == CurrentSlot)
	{
		SelectedWeaponPos++;
	}
	else
	{
		CurrentSlot = slot;
		SelectedWeaponPos = 0;
	}
	TArray< FName>* slotPos = WeaponCanEquip.Find(CurrentSlot);

	if (slotPos->Num() > 0)
	{
		if (SelectedWeaponPos >= slotPos->Num())
		{
			SelectedWeaponPos = 0;
		}
	}
	else
	{
		SelectedWeaponPos = -1;
	}

	int32 slotint = 0;
	switch (slot)
	{
	case EWeaponSlots::Slot1:
		slotint = 1;
		break;
	case EWeaponSlots::Slot2:
		slotint = 2;
		break;
	case EWeaponSlots::Slot3:
		slotint = 3;
		break;
	case EWeaponSlots::Slot4:
		slotint = 4;
		break;
	case EWeaponSlots::Slot5:
		slotint = 5;
		break;
	}

	IInterracWithCharInterface::Execute_UpdateWeaponSlotsWidget(GetOwner(), SelectMode, slotint, SelectedWeaponPos, *slotPos);

}

void UWeaponComponent::EquipWeapon()
{
	if (SelectedWeaponPos > -1)
	{
		SelectedWeaponId  = WeaponCanEquip[CurrentSlot][SelectedWeaponPos];
		FWeaponData* SelectedWeaponData = AllWeapon.Find(SelectedWeaponId);

		//��� ������� ��������� ������ � �����������
		if (SelectedWeapon)
		{
			SelectedWeapon->Destroy();
		}

		SelectedWeapon = nullptr;
		ShowAmmoPanel = false;
		IInterracWithCharInterface::Execute_ShowCross(GetOwner(),false);
		IInterracWithCharInterface::Execute_UpdateReloadBar(GetOwner(), false, 1);

		if (SelectedWeaponData->WeaponClass)
		{
			FVector point;
			SelectedWeapon = Cast<AMaineWeapon>(GetWorld()->SpawnActor(SelectedWeaponData->WeaponClass, &point));
			ShowAmmoPanel = true;
			UArrowComponent* AttachComponent = nullptr;
			IInterracWithCharInterface::Execute_GetArrowComponent(GetOwner(), AttachComponent);

			SelectedWeapon->SetOwner(GetOwner());
			SelectedWeapon->AttachToComponent(AttachComponent, FAttachmentTransformRules::SnapToTargetIncludingScale, AttachComponent->GetAttachSocketName());
			IInterracWithCharInterface::Execute_ShowCross(GetOwner(), true);
		}
		IInterracWithCharInterface::Execute_UpdateAmmoWidget(GetOwner(), ShowAmmoPanel, SelectedWeaponData->CageCountAmmo, SelectedWeaponData->AmmoCount);
	}

	SelectMode = false;
	SelectedWeaponPos = -1;

	TArray< FName>* slotPos = WeaponCanEquip.Find(CurrentSlot);
	IInterracWithCharInterface::Execute_UpdateWeaponSlotsWidget(GetOwner(), SelectMode, 0, SelectedWeaponPos, *slotPos);
}

void UWeaponComponent::WeaponFire()
{
	if (SelectedWeapon)
	{
		FWeaponData* SelectedWeaponData = AllWeapon.Find(SelectedWeaponId);
		SelectedWeapon->StartFire(SelectedWeaponData->CageMaxAmmo, SelectedWeaponData->CageCountAmmo, SelectedWeaponData->MaxAmmo, SelectedWeaponData->AmmoCount);
		IInterracWithCharInterface::Execute_UpdateAmmoWidget(GetOwner(), ShowAmmoPanel, SelectedWeaponData->CageCountAmmo, SelectedWeaponData->AmmoCount);
	}
}

void UWeaponComponent::UpdateWeaponWidgets()
{
	if (SelectedWeapon && ShowAmmoPanel)
	{
		IInterracWithCharInterface::Execute_ShowCross(GetOwner(), true);
		FWeaponData* SelectedWeaponData = AllWeapon.Find(SelectedWeaponId);
		IInterracWithCharInterface::Execute_UpdateAmmoWidget(GetOwner(), ShowAmmoPanel, SelectedWeaponData->CageCountAmmo, SelectedWeaponData->AmmoCount);
	}
}

void UWeaponComponent::DestroyWeapon()
{
	if (SelectedWeapon)
	{
		SelectedWeapon->Destroy();
	}
}

