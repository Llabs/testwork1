// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/MaineGameInstance.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "Player/PlayerCharacter.h"
#include "GameFramework/PlayerController.h"



UMaineGameInstance::UMaineGameInstance()
{
	TArray< FName> list1;
	TArray< FName> list2;
	TArray< FName> list3;
	TArray< FName> list4;
	TArray< FName> list5;

	WeaponCanEquip.Add(EWeaponSlots::Slot1, list1);
	WeaponCanEquip.Add(EWeaponSlots::Slot2, list2);
	WeaponCanEquip.Add(EWeaponSlots::Slot3, list3);
	WeaponCanEquip.Add(EWeaponSlots::Slot4, list4);
	WeaponCanEquip.Add(EWeaponSlots::Slot5, list5);
}

void UMaineGameInstance::Respawn(APlayerController* controller)
{
	FVector SpawnLocation = FVector(0.f, 0.f, 0.f);
	FRotator SpawnRotation = FRotator(0.f, 0.f, 0.f);
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(controller->GetWorld(), APlayerStart::StaticClass(), FoundActors);
	SpawnLocation = FoundActors[0]->GetActorLocation();
	SpawnLocation += FVector(0, 0, 30);

	APlayerCharacter* NewPlayer = Cast<APlayerCharacter>(controller->GetWorld()->SpawnActor(PlauerCharClass, &SpawnLocation));
	controller->EnableInput(controller);
	controller->Possess(NewPlayer);
}

void UMaineGameInstance::SpawnNewPlayer_Implementation(APlayerController* controller)
{
	Respawn(controller);
}

void UMaineGameInstance::SaveWeaponCanEquip_Implementation(EWeaponSlots SlotVal, const TArray<FName>& WeaponCanEquipVal)
{
	WeaponCanEquip[SlotVal] = WeaponCanEquipVal;
}


void UMaineGameInstance::SaveWeaponSelectInfo_Implementation(const TMap<FName, FWeaponData>& AllWeaponVal, const FName& SelectedWeaponIdVal, bool ShowAmmoPanelVal, bool instanceDataVal)
{
	AllWeapon = AllWeaponVal;
	SelectedWeaponId = SelectedWeaponIdVal;
	ShowAmmoPanel = ShowAmmoPanelVal;
	instanceData = instanceDataVal;
}

void UMaineGameInstance::LoadWeaponEquip_Implementation(EWeaponSlots SlotVal, TArray<FName>& WeaponCanEquipVal)
{
	WeaponCanEquipVal = WeaponCanEquip[SlotVal];
}



void UMaineGameInstance::LoadWeaponSelectInfo_Implementation(TMap<FName, FWeaponData>& AllWeaponVal, FName& SelectedWeaponIdVal, bool& ShowAmmoPanelVal, bool& instanceDataVal)
{
	AllWeaponVal = AllWeapon;
	SelectedWeaponIdVal = SelectedWeaponId;
	ShowAmmoPanelVal = ShowAmmoPanel;
	instanceDataVal = instanceData;
}

void UMaineGameInstance::ClearInstanceWeaponData_Implementation()
{
	AllWeapon.Empty();
	SelectedWeaponId = FName();
	ShowAmmoPanel = false;
	instanceData = false;

	WeaponCanEquip[EWeaponSlots::Slot1].Empty();
	WeaponCanEquip[EWeaponSlots::Slot2].Empty();
	WeaponCanEquip[EWeaponSlots::Slot3].Empty();
	WeaponCanEquip[EWeaponSlots::Slot4].Empty();
	WeaponCanEquip[EWeaponSlots::Slot5].Empty();
	
}

void UMaineGameInstance::SaveHealthData_Implementation(int32 HealthVal, bool instanceDataVal)
{
	health = HealthVal;
	healthInstanceData = instanceDataVal;
}

void UMaineGameInstance::LoadHealthData_Implementation(int32& HealthVal, bool& instanceDataVal)
{
	HealthVal = health;
	instanceDataVal = healthInstanceData;
}
