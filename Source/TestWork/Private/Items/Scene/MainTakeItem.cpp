// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/Scene/MainTakeItem.h"
#include "Components/BoxComponent.h"
#include "Interface/InterracWithCharInterface.h"

// Sets default values
AMainTakeItem::AMainTakeItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetupAttachment(GetRootComponent());
	BoxCollision->SetGenerateOverlapEvents(true);
	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &AMainTakeItem::OnOverlapBegin);
	BoxCollision->OnComponentEndOverlap.AddDynamic(this, &AMainTakeItem::OnOverlapEnd);

}

// Called when the game starts or when spawned
void AMainTakeItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMainTakeItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMainTakeItem::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<IInterracWithCharInterface>(OtherActor))
	{
		IInterracWithCharInterface::Execute_SelectItem(OtherActor, this);
	}
}

void AMainTakeItem::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (Cast<IInterracWithCharInterface>(OtherActor))
	{
		IInterracWithCharInterface::Execute_ClearSelectItem(OtherActor);
	}
}

void AMainTakeItem::UseItem_Implementation(AActor* item)
{
}

