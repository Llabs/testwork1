// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/Weapon/WeaponType1.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/ActorComponent.h"
#include "Components/SceneComponent.h"
#include "Interface/CombatInterface.h"
//#include "GameFramework/Actor.h"


void AWeaponType1::FireEvent()
{
    FVector ArrowPoint = FireArrow->GetComponentLocation();
    FVector ArrowForward = FireArrow->GetForwardVector();

    TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
    // TraceObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery7);
    TraceObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery8);
    const TArray<AActor*> ActorsToIgnore;
    EDrawDebugTrace::Type DrawDebugType = EDrawDebugTrace::Type::ForDuration;
    FLinearColor TraceColor = FLinearColor::Red;
    FLinearColor TraceHitColor = FLinearColor::Green;
    TArray<FVector> traceStartPoints;

    UCameraComponent* CameraRef = Cast< UCameraComponent> (GetOwner()->GetComponentByClass(UCameraComponent::StaticClass()));

    FHitResult TraceHitResult;
      if (CameraRef)
      {
          FVector  CameraForvard = CameraRef->GetForwardVector();
          FVector   CameraLocation = CameraRef->GetComponentLocation();
          FVector End = CameraLocation + (CameraForvard * 1000);
         
          FVector Start = ArrowPoint + (ArrowForward * 5);
          UKismetSystemLibrary::LineTraceSingleForObjects(GetWorld(), Start, End, TraceObjectTypes, true,
              ActorsToIgnore, DrawDebugType, TraceHitResult, true, TraceColor,
              TraceHitColor, 3.0f);
      }
      else
      {
          FVector Start = ArrowPoint + (ArrowForward * 5);
          FVector End = ArrowPoint + (ArrowForward * 100);
          UKismetSystemLibrary::LineTraceSingleForObjects(GetWorld(), Start, End, TraceObjectTypes, true,
              ActorsToIgnore, DrawDebugType, TraceHitResult, true, TraceColor,
              TraceHitColor, 3.0f);
      }

      if (TraceHitResult.Actor.Get())
      {
          AActor* target = TraceHitResult.Actor.Get();
          if (target)
          {
              if (Cast<ICombatInterface>(target))
              {
                  ICombatInterface::Execute_AddDamage(TraceHitResult.Actor.Get(), 10);
              }
          }
      }   
}
