// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/Weapon/MaineWeapon.h"
#include "Components/ArrowComponent.h"
#include "Components/ActorComponent.h"
#include "Interface/InterracWithCharInterface.h"

// Sets default values
AMaineWeapon::AMaineWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CenterArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("CenterArrow"));
	CenterArrow->SetupAttachment(RootComponent);

	FireArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("FireArrow"));
	FireArrow->SetupAttachment(CenterArrow);
}

// Called when the game starts or when spawned
void AMaineWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMaineWeapon::UpdateReloadData()
{
	currentReloadTime += 0.01f;

	if (currentReloadTime >= reloadTime)
	{
		IInterracWithCharInterface::Execute_UpdateReloadBar(GetOwner(), false, 0);
		isReloading = false;
		ReloadTimerHandle.Invalidate();
	}
	else
	{
		IInterracWithCharInterface::Execute_UpdateReloadBar(GetOwner(), true, currentReloadTime / reloadTime);
	}
}

void AMaineWeapon::fireUnloc()
{
	fireUnlocFlag = true;
}

// Called every frame
void AMaineWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMaineWeapon::FireEvent()
{
}

void AMaineWeapon::ReloadStart(const int32& maxChageAmmoVal, int32& currentChageAmmoVal, const int32& maxAmmoVal, int32& CurrentAmmoVal)
{
	if (!isReloading && CurrentAmmoVal)
	{
		GetWorldTimerManager().SetTimer(ReloadTimerHandle, this, &AMaineWeapon::UpdateReloadData, 0.01f, true);
		currentReloadTime = 0;
		isReloading = true;

		// ��� ���� � ��������
		if (currentChageAmmoVal == 0) 
		{
			// � ������ ���� ������ ��� ���������� � �������
			if (CurrentAmmoVal >= maxChageAmmoVal) 
			{
				// �������� ������� �� ���������
				currentChageAmmoVal = maxChageAmmoVal; 
				// ������� ����������� ���� �� ����� �����
				CurrentAmmoVal -= maxChageAmmoVal; 
			}
			// ����� ������� � ���� �� � ��� ������ ���� � �����
			else if (CurrentAmmoVal > 0) 
			{
				// ������������� ������� � �������
				currentChageAmmoVal = CurrentAmmoVal; 
				// ������� 0
				CurrentAmmoVal = 0; 
			}
		}
		// ����������� ����� � �������� ���� �������
		else 
		{
			// ������� � ���� �� � ��� ������ ���� � �����
			if (CurrentAmmoVal > 0) 
			{
				int32 pat = maxChageAmmoVal - currentChageAmmoVal;
				// � ����� ���� ������ ��� ����� �������� � �������
				if (CurrentAmmoVal >= pat) 
				{
					// ��������� ���� 
					currentChageAmmoVal += pat; 
					// ������� �� ����� �����
					CurrentAmmoVal -= pat;
				}
				else
				{
					// ������������� ������� � �������
					currentChageAmmoVal += CurrentAmmoVal; 
					CurrentAmmoVal = 0;
				}
			}
		}
	}
}


void AMaineWeapon::StartFire(const int32& maxChageAmmoVal, int32& currentChageAmmoVal, const int32& maxAmmoVal, int32& CurrentAmmoVal)
{
	if (fireUnlocFlag && !isReloading)
	{
		GetWorldTimerManager().SetTimer(FireUnlocHandle, this, &AMaineWeapon::fireUnloc, fireTime, false);
		fireUnlocFlag = false;
		if (currentChageAmmoVal == 0)
		{
			ReloadStart(maxChageAmmoVal, currentChageAmmoVal, maxAmmoVal, CurrentAmmoVal);
		}
		else
		{
			currentChageAmmoVal--;
			FireEvent();
		}
	}

}






