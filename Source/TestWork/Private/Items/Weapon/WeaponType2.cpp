// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/Weapon/WeaponType2.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"

AWeaponType2::AWeaponType2()
{
    reloadTime = 5;
}

void AWeaponType2::FireEvent()
{
    FVector ArrowPoint = FireArrow->GetComponentLocation();
    FVector ArrowForward = FireArrow->GetForwardVector();

    UCameraComponent* CameraRef = Cast< UCameraComponent>(GetOwner()->GetComponentByClass(UCameraComponent::StaticClass()));

    FHitResult TraceHitResult;
    if (CameraRef)
    {
        FVector  CameraForvard = CameraRef->GetForwardVector();
        FVector   CameraLocation = CameraRef->GetComponentLocation();
        FVector End = CameraLocation + (CameraForvard * 1500);

        FVector Start = ArrowPoint + (ArrowForward * 5);
        FActorSpawnParameters SpawnParams;
        SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

        FRotator ProjecttileRot = UKismetMathLibrary::FindLookAtRotation(Start, End);

        AActor* NewProjecttile = Cast<AActor>(GetWorld()->SpawnActor(Projectile, &Start, &ProjecttileRot, SpawnParams));

        


    }
    else
    {
        FVector Start = ArrowPoint + (ArrowForward * 5);
        FActorSpawnParameters SpawnParams;
        SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;


        AActor* NewProjecttile = Cast<AActor>(GetWorld()->SpawnActor(Projectile, &Start));
    }

}
