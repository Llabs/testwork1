// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/MaineUserWidget.h"

void UMaineUserWidget::UpdateWeaponSlotsWidget_Implementation(bool selectMod, int32 SelectedSlot, int32 SelectedWeapon, const TArray<FName>& weaponisslot)
{
}

void UMaineUserWidget::UpdateAmmoWidget_Implementation(bool ShowPanel, int32 CageCountAmmoVal, int32 CountAmmoVal)
{
}

void UMaineUserWidget::ShowCross_Implementation(bool ShowPanel)
{
}

void UMaineUserWidget::UpdateHealth_Implementation(int32 HealthVal)
{
}

void UMaineUserWidget::ShowDeathWidget_Implementation()
{
}

void UMaineUserWidget::UpdateReloadBar_Implementation(bool ShowBar, float percent)
{
}

void UMaineUserWidget::ShowInterractionPanel_Implementation(bool ShowPanel)
{
}

void UMaineUserWidget::SelectMap_Implementation(int32 value)
{
}

void UMaineUserWidget::OpenMapWidget_Implementation()
{
}

void UMaineUserWidget::LoadMap_Implementation(APlayerCharacter* PlayerRef)
{
}
