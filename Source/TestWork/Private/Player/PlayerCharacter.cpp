// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Camera/CameraComponent.h"
#include "UI/MaineUserWidget.h"
#include "Component/WeaponComponent.h"
#include "Components/ArrowComponent.h"
#include "Component/HealthComponent.h"
#include "Interface/InteractionInterface.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"
#include "Component/PlayerWeaponComponent.h"
#include "Component/PlayerHealthComponent.h"
#include "Core/MaineGameInstance.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	//��������� �������
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// ��������� ������
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f));
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	WeaponComponent = CreateDefaultSubobject< UPlayerWeaponComponent>(TEXT("WeaponComponent"));
	WeaponAttach = CreateDefaultSubobject<UArrowComponent>(TEXT("WeaponAttach"));
	WeaponAttach->SetupAttachment(GetCapsuleComponent());

	HeathComponent = CreateDefaultSubobject<UPlayerHealthComponent>(TEXT("HealthComponent"));
	

}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	UWidgetLayoutLibrary::RemoveAllWidgets(GetWorld());
	
	MainWidget = CreateWidget<UMaineUserWidget>(this->GetGameInstance(), MaineWidgetClass);

	MainWidget->AddToViewport(9999);

	WeaponComponent->UpdateWeaponWidgets();

	HeathComponent->ChangeHealth(100);

	HeathComponent->LoadDataFromInstance();

}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// ��������� ������
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
		Super::SetupPlayerInputComponent(PlayerInputComponent);

		// ������
		PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
		PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

		// ��������
		PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
		PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
		PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
		PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

		// ������������� ������ � ���������
		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APlayerCharacter::OnUseWeapon);
		PlayerInputComponent->BindAction("UseItem", IE_Pressed, this, &APlayerCharacter::UseItemEvent);

		//����� ������
		PlayerInputComponent->BindAction("1", IE_Pressed, this, &APlayerCharacter::SelectSlot1);
		PlayerInputComponent->BindAction("2", IE_Pressed, this, &APlayerCharacter::SelectSlot2);
		PlayerInputComponent->BindAction("3", IE_Pressed, this, &APlayerCharacter::SelectSlot3);
		PlayerInputComponent->BindAction("4", IE_Pressed, this, &APlayerCharacter::SelectSlot4);
		PlayerInputComponent->BindAction("5", IE_Pressed, this, &APlayerCharacter::SelectSlot5);

		//����� ������
		PlayerInputComponent->BindAction("UpMap", IE_Pressed, this, &APlayerCharacter::UpMap);
		PlayerInputComponent->BindAction("DownMap", IE_Pressed, this, &APlayerCharacter::DownMap);
		PlayerInputComponent->BindAction("OpenMap", IE_Pressed, this, &APlayerCharacter::OpenMap);
		PlayerInputComponent->BindAction("SelectMap", IE_Pressed, this, &APlayerCharacter::SelectMap);

}

void APlayerCharacter::OnUseWeapon()
{
	if (WeaponComponent->GetSelectMode())
	{
		WeaponComponent->EquipWeapon();
	}
	else
	{
		WeaponComponent->WeaponFire();
	}
}

void APlayerCharacter::UseItemEvent()
{
	if (selectedUseItem)
	{
		IInteractionInterface::Execute_UseItem(selectedUseItem, this);
	}
}

void APlayerCharacter::SelectSlot1()
{
	WeaponComponent->SelectWeapon(EWeaponSlots::Slot1);
}

void APlayerCharacter::SelectSlot2()
{
	WeaponComponent->SelectWeapon(EWeaponSlots::Slot2);
}

void APlayerCharacter::SelectSlot3()
{
	WeaponComponent->SelectWeapon(EWeaponSlots::Slot3);
}

void APlayerCharacter::SelectSlot4()
{
	WeaponComponent->SelectWeapon(EWeaponSlots::Slot4);
}

void APlayerCharacter::SelectSlot5()
{
	WeaponComponent->SelectWeapon(EWeaponSlots::Slot5);
}

void APlayerCharacter::MoveForward(float Val)
{
	if (Val != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Val);
	}
}

void APlayerCharacter::MoveRight(float Val)
{
	if (Val != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Val);
	}
}

void APlayerCharacter::Death()
{
	APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	DisableInput(controller);
	if (MainWidget)
	{
		MainWidget->ShowDeathWidget();
	}
		GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &APlayerCharacter::Respawn, 1.0f, false);
}

void APlayerCharacter::Respawn()
{
	UMaineGameInstance* instance = Cast<UMaineGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	IInstsnceInterface::Execute_SpawnNewPlayer(UGameplayStatics::GetGameInstance(GetWorld()), controller);

	WeaponComponent->DestroyWeapon();

	Destroy();
	
}

void APlayerCharacter::UpMap()
{
	if (MainWidget)
	{
		MainWidget->SelectMap(1);
	}
}

void APlayerCharacter::DownMap()
{
	if (MainWidget)
	{
		MainWidget->SelectMap(-1);
	}
}

void APlayerCharacter::OpenMap()
{
	if (MainWidget)
	{
		MainWidget->OpenMapWidget();
	}
}

void APlayerCharacter::SelectMap()
{
	if (MainWidget)
	{
		MainWidget->LoadMap(this);
	}
}

void APlayerCharacter::UpdateWeaponSlotsWidget_Implementation(bool selectMod, int32 SelectedSlot, int32 SelectedWeapon, const TArray<FName>& weaponisslot)
{
	if (MainWidget)
	{
		MainWidget->UpdateWeaponSlotsWidget(selectMod, SelectedSlot, SelectedWeapon, weaponisslot);
	}
}

void APlayerCharacter::UpdateAmmoWidget_Implementation(bool ShowPanel, int32 CageCountAmmoVal, int32 CountAmmoVal)
{
	if (MainWidget)
	{
		MainWidget->UpdateAmmoWidget(ShowPanel, CageCountAmmoVal, CountAmmoVal);
	}
}

void APlayerCharacter::ShowCross_Implementation(bool ShowPanel)
{
	if (MainWidget)
	{
		MainWidget->ShowCross(ShowPanel);
	}
}

void APlayerCharacter::UpdateHealth_Implementation(int32 HealthVal)
{
	if (MainWidget)
	{
		MainWidget->UpdateHealth(HealthVal);
	}
}

void APlayerCharacter::UpdateReloadBar_Implementation(bool ShowBar, float percent)
{
	if (MainWidget)
	{
		MainWidget->UpdateReloadBar(ShowBar, percent);
	}
}

void APlayerCharacter::GetArrowComponent_Implementation(UArrowComponent*& component)
{
	component = WeaponAttach;
}

void APlayerCharacter::SelectItem_Implementation(AActor* item)
{
	selectedUseItem = item;

	if (MainWidget)
	{
		MainWidget->ShowInterractionPanel(true);
	}
}

void APlayerCharacter::GetSelectItem_Implementation(AActor*& item)
{
	item = selectedUseItem;
}

void APlayerCharacter::ClearSelectItem_Implementation()
{
	selectedUseItem = nullptr;

	if (MainWidget)
	{
		MainWidget->ShowInterractionPanel(false);
	}
}

void APlayerCharacter::AddDamage_Implementation(int32 damage)
{
	int32 lDamage = damage;
	if (lDamage > 0)
	{
		lDamage *= -1;
	}

	HeathComponent->ChangeHealth(lDamage);
}

void APlayerCharacter::DeathEvent_Implementation()
{
	Death();
}



