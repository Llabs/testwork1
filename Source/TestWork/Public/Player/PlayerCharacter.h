// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interface/InterracWithCharInterface.h"
#include "Interface/CombatInterface.h"
#include "PlayerCharacter.generated.h"

class UMaineUserWidget;


/** �������� ������� �������� */

UCLASS()
class TESTWORK_API APlayerCharacter : public ACharacter, public IInterracWithCharInterface, public ICombatInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


public:
	/** ��������� �������� � ����/����� */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;
	/** ��������� �������� � �����/���� */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;
	/** ������ ������*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		class UCameraComponent* FirstPersonCameraComponent;
	/** ��������� ������*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
		class  UPlayerWeaponComponent* WeaponComponent;
	/** ��������� � �������� ������������� ��� ������*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
		class UArrowComponent* WeaponAttach;
	/** ��������� ��������*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
		class UPlayerHealthComponent* HeathComponent;
	/** ����� ���������� ������������*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MaineWidget)
	TSubclassOf<UMaineUserWidget> MaineWidgetClass;



protected:
	/** ��������� ������������ */
	UMaineUserWidget* MainWidget = nullptr;
	/** ������� �� ����� ��� �������������� */
	AActor* selectedUseItem = nullptr;
	/** ������ ����� ��������� ������ */
	FTimerHandle SpawnTimerHandle;

	/** ������������� ������ */
	void OnUseWeapon();
	/** ������������� ��������*/
	void UseItemEvent();

	/** ����� ������� ����� ������*/
	void SelectSlot1();
	/** ����� ������� ����� ������*/
	void SelectSlot2();
	/** ����� �������� ����� ������*/
	void SelectSlot3();
	/** ����� ��������� ����� ������*/
	void SelectSlot4();
	/** ����� ������ ����� ������*/
	void SelectSlot5();

	/** �������� �����/�����*/
	void MoveForward(float Val);
	/** �������� � ����/����� */
	void MoveRight(float Val);

	/** ������ ��������� */
	void Death();
	/** ������� ��������� */
	void Respawn();

	/** ������������� ������ ���� ����� */
	void UpMap();
	/** ������������� ������ ���� ���� */
	void DownMap();
	/** ��������� ������ ���� */
	void OpenMap();
	/** �������� ��������� ����� */
	void SelectMap();

/*������ ���������� IInterracWithCharInterface*/
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UpdateWeaponSlotsWidget(bool selectMod, int32 SelectedSlot, int32 SelectedWeapon, const TArray<FName>& weaponisslot);
		void UpdateWeaponSlotsWidget_Implementation(bool selectMod, int32 SelectedSlot, int32 SelectedWeapon, const TArray<FName>& weaponisslot);


		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
			void UpdateAmmoWidget(bool ShowPanel, int32 CageCountAmmoVal, int32 CountAmmoVal);
			void UpdateAmmoWidget_Implementation(bool ShowPanel, int32 CageCountAmmoVal, int32 CountAmmoVal);
		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
			void ShowCross(bool ShowPanel);
			void ShowCross_Implementation(bool ShowPanel);
		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
			void UpdateHealth(int32 HealthVal);
			void UpdateHealth_Implementation(int32 HealthVal);

		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
			void UpdateReloadBar(bool ShowBar, float percent);
			void UpdateReloadBar_Implementation(bool ShowBar, float percent);


		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void GetArrowComponent(UArrowComponent*& component);
		void GetArrowComponent_Implementation(UArrowComponent*& component);

		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
			void SelectItem(AActor* item);
			void SelectItem_Implementation(AActor* item);

		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
			void GetSelectItem(AActor*& item);
			void GetSelectItem_Implementation(AActor*& item);

		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
			void ClearSelectItem();
			void ClearSelectItem_Implementation();

/*������ ���������� ICombatInterface */
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddDamage(int32 damage);
		void AddDamage_Implementation(int32 damage);

		UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
			void DeathEvent();
		void DeathEvent_Implementation();
};
