// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Component/WeaponComponent.h"
#include "PlayerWeaponComponent.generated.h"

/**
 * 
 */
UCLASS()
class TESTWORK_API UPlayerWeaponComponent : public UWeaponComponent
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	/* ���������� ������ � ������� */
	UFUNCTION(BlueprintCallable)
	void SaveDataToInstance();
	
};
