// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FunctionLib/WeaponLibrary.h"
#include "WeaponComponent.generated.h"





UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTWORK_API UWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;




protected:
	/* �� ��������� ������*/
	UPROPERTY(EditAnywhere)
	TMap< FName, FWeaponData> AllWeapon;
	/* ������ ������� ����� ���� ������������ */
	TMap< EWeaponSlots, TArray< FName>> WeaponCanEquip;
	/* ��������� ������*/
	AMaineWeapon* SelectedWeapon = nullptr; 
	/* ID ���������� ������*/
	FName SelectedWeaponId;
	/* ������������ ������ ��������*/
	bool ShowAmmoPanel = false;

	/* ������� ����*/
	EWeaponSlots CurrentSlot;
	/* ��������� ������� ������ �����*/
	int8 SelectedWeaponPos = -1; 
	/* ����� ������ ������ */
	bool SelectMode = false;                 

public:
	/* ��������� ������ */
	UFUNCTION(BlueprintCallable)
	void AddNewWeapon(FName WeaponId);
	/* ��������� ������� */
	UFUNCTION(BlueprintCallable)
	void AddAmmo(FName WeaponId, int32 ammo);

	/* �������� ������ */
	void SelectWeapon(EWeaponSlots slot);

	/* ��������� ������ */
	void EquipWeapon();
	/* ������ ������� */
	void WeaponFire();
		
	/* ��������� ��������� �� ����� � ������ ������ ������ */
	bool GetSelectMode() {
		return SelectMode;
	};

	/* ��������� ������ ������ */
	void UpdateWeaponWidgets();
	/* ������� ������*/
	void DestroyWeapon();
};
