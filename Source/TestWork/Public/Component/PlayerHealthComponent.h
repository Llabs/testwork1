// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Component/HealthComponent.h"
#include "PlayerHealthComponent.generated.h"

/**
 * 
 */
UCLASS()
class TESTWORK_API UPlayerHealthComponent : public UHealthComponent
{
	GENERATED_BODY()

public:
	/* ��������� ������ �� ��������*/
	void LoadDataFromInstance();
	/* ���������� ������ � ������� */
	UFUNCTION(BlueprintCallable)
	void SaveDataToInstance();
	
};
