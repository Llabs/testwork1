// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "WeaponLibrary.generated.h"

/**
 * 
 */

class AMaineWeapon;

 //�����
UENUM(BlueprintType)
enum class EWeaponSlots : uint8
{
	Slot1,
	Slot2,
	Slot3,
	Slot4,
	Slot5
};


//��������� ������ ������
USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName WeaponName;                          //�������� ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EWeaponSlots WeaponSlot;                          // ���� ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf <AMaineWeapon> WeaponClass;  //����� ������

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxAmmo = 0;                     //�������� ����
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 AmmoCount = 0;                     //����������� ����

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 CageMaxAmmo = 0;                   //�������� ���� � ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 CageCountAmmo = 0; //����������� ���� � ������
};


UCLASS()
class TESTWORK_API UWeaponLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
