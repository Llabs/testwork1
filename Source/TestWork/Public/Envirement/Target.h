// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/CombatInterface.h"
#include "Target.generated.h"

UCLASS()
class TESTWORK_API ATarget : public AActor, public ICombatInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATarget();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*������ ���������� ICombatInterface */
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddDamage(int32 damage);
	void AddDamage_Implementation(int32 damage);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DeathEvent();
	void DeathEvent_Implementation();

};
