// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Interface/InstsnceInterface.h"
#include "MaineGameInstance.generated.h"

/**
 * 
 */

class APlayerCharacter;

UCLASS()
class TESTWORK_API UMaineGameInstance : public UGameInstance, public IInstsnceInterface
{
	GENERATED_BODY()

public:
	UMaineGameInstance();

protected:
	//������ ������
	TMap<FName, FWeaponData>AllWeapon;
	TMap< EWeaponSlots, TArray< FName>> WeaponCanEquip; // ������ ������� ����� ���� ������������
	FName SelectedWeaponId;
	bool ShowAmmoPanel;
	bool instanceData;

	//�������������� 
	int32 health;
	bool healthInstanceData;

	/* ����������� ������*/
	void Respawn(APlayerController* controller);
		
public:
	/* ����� ������ */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Player)
	TSubclassOf<APlayerCharacter> PlauerCharClass;
	
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SpawnNewPlayer(APlayerController* controller);
		void SpawnNewPlayer_Implementation(APlayerController* controller);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SaveWeaponCanEquip(EWeaponSlots SlotVal, const TArray< FName >& WeaponCanEquipVal);
		void SaveWeaponCanEquip_Implementation(EWeaponSlots SlotVal, const TArray< FName >& WeaponCanEquipVal);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SaveWeaponSelectInfo(const TMap< FName, FWeaponData>& AllWeaponVal, const FName& SelectedWeaponIdVal, bool ShowAmmoPanelVal, bool instanceDataVal);
		void SaveWeaponSelectInfo_Implementation(const TMap< FName, FWeaponData>& AllWeaponVal, const FName& SelectedWeaponIdVal, bool ShowAmmoPanelVal, bool instanceDataVal);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void LoadWeaponEquip(EWeaponSlots SlotVal, TArray< FName >& WeaponCanEquipVal);
		void LoadWeaponEquip_Implementation(EWeaponSlots SlotVal, TArray< FName >& WeaponCanEquipVal);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void LoadWeaponSelectInfo(TMap< FName, FWeaponData>& AllWeaponVal, FName& SelectedWeaponIdVal, bool& ShowAmmoPanelVal, bool& instanceDataVal);
		void LoadWeaponSelectInfo_Implementation(TMap< FName, FWeaponData>& AllWeaponVal, FName& SelectedWeaponIdVal, bool& ShowAmmoPanelVal, bool& instanceDataVal);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ClearInstanceWeaponData();
		void ClearInstanceWeaponData_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SaveHealthData(int32 HealthVal, bool instanceDataVal);
		void SaveHealthData_Implementation(int32 HealthVal, bool instanceDataVal);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void LoadHealthData(int32& HealthVal, bool& instanceDataVal);
		void LoadHealthData_Implementation(int32& HealthVal, bool& instanceDataVal);
	
};
