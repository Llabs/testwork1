// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MaineUserWidget.generated.h"

/**
 * 
 */


class APlayerCharacter;


UCLASS()
class TESTWORK_API UMaineUserWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UCanvasPanel* MainPanel;

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UpdateWeaponSlotsWidget(bool selectMod, int32 SelectedSlot, int32 SelectedWeapon, const TArray<FName>& weaponisslot);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UpdateAmmoWidget(bool ShowPanel, int32 CageCountAmmoVal, int32 CountAmmoVal);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ShowCross(bool ShowPanel);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UpdateHealth(int32 HealthVal);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ShowDeathWidget();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UpdateReloadBar(bool ShowBar, float percent);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ShowInterractionPanel(bool ShowPanel);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SelectMap(int32 value);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void LoadMap(APlayerCharacter * PlayerRef);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OpenMapWidget();
	
};
