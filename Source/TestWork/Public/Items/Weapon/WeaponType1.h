// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/Weapon/MaineWeapon.h"
#include "WeaponType1.generated.h"

/**
 * 
 */
UCLASS()
class TESTWORK_API AWeaponType1 : public AMaineWeapon
{
	GENERATED_BODY()
	
public:

	virtual void FireEvent() override;
};
