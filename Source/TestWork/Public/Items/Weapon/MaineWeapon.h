// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MaineWeapon.generated.h"


UCLASS()
class TESTWORK_API AMaineWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMaineWeapon();
	/*����������� ��������� � �������� �������� ���������*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
		class UArrowComponent* CenterArrow;

	/* ��������� ���������� ��������� ����� �������� */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
		class UArrowComponent* FireArrow;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* ����� ����������� ������*/
	float reloadTime = 1;
	/* ������� ����� �����������. � ������ ����������� 0 ����� ������������� �� reloadTime ����� ���� ����������� ���������*/
	float currentReloadTime;
	/* ������ ����� �����������*/
	FTimerHandle ReloadTimerHandle;
	/* ���� �������� ����������� */
	void UpdateReloadData();
	/* ���������� ��� ��� �����������*/
	bool isReloading = false;

	/* ������ �������� ����� ����������*/
	FTimerHandle FireUnlocHandle;
	/* ����� ����� ����������*/
	float fireTime = 0.1;
	/* ����� ��� �� ����� ��������*/
	bool fireUnlocFlag = true;
	/* ��������� �������� ����� */
	void fireUnloc();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* ������� */
	virtual void FireEvent();

	/* ����������� */
	void ReloadStart(const int32& maxChageAmmoVal, int32& currentChageAmmoVal, const int32& maxAmmoVal, int32& CurrentAmmoVal);

	/* ������ ��������*/
	void StartFire(const int32& maxChageAmmoVal, int32& currentChageAmmoVal, const int32& maxAmmoVal, int32& CurrentAmmoVal);

};
