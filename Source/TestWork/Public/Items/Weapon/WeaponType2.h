// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/Weapon/MaineWeapon.h"
#include "WeaponType2.generated.h"

/**
 * 
 */
UCLASS()
class TESTWORK_API AWeaponType2 : public AMaineWeapon
{
	GENERATED_BODY()

public:

	AWeaponType2();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Player)
		TSubclassOf<AActor> Projectile;


	virtual void FireEvent() override;
	
};
