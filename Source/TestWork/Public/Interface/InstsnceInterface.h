// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FunctionLib/WeaponLibrary.h"
#include "InstsnceInterface.generated.h"

class APlayerCharacter;


// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInstsnceInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TESTWORK_API IInstsnceInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	/* ������� ������ */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SpawnNewPlayer(APlayerController* controller);

	/* ��������� ������ ������ ������� ����� ������������ ����� � �������*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SaveWeaponCanEquip(EWeaponSlots SlotVal, const TArray< FName > &WeaponCanEquipVal);
	/* ��������� ���������� � ��������� ������  � ������� */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SaveWeaponSelectInfo(const TMap< FName, FWeaponData>& AllWeaponVal, const FName& SelectedWeaponIdVal, bool ShowAmmoPanelVal, bool instanceDataVal);
	/* ��������� ������ ������ ������� ����� ������������ ����� �� ��������*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void LoadWeaponEquip(EWeaponSlots SlotVal, TArray< FName >& WeaponCanEquipVal);
	/* ��������� ���������� � ��������� ������  �� �������� */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void LoadWeaponSelectInfo(TMap< FName, FWeaponData>& AllWeaponVal, FName& SelectedWeaponIdVal, bool &ShowAmmoPanelVal, bool & instanceDataVal);
	/* ������� ���� ����������� ������� ������ � ��������*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ClearInstanceWeaponData();                        


	/* ��������� ���������� � ��������� ��������  � ������� */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SaveHealthData(int32 HealthVal, bool instanceDataVal);
	/* ��������� ���������� � ��������� ��������  �� �������� */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void LoadHealthData(int32 &HealthVal, bool &instanceDataVal);

};
