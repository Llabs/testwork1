// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InterracWithCharInterface.generated.h"


// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInterracWithCharInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TESTWORK_API IInterracWithCharInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/* ��������� ������ � ������� ������ */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UpdateWeaponSlotsWidget(bool selectMod, int32 SelectedSlot, int32 SelectedWeapon, const TArray<FName> & weaponisslot);

	/*��������� ������ � ���������*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UpdateAmmoWidget(bool ShowPanel, int32 CageCountAmmoVal, int32 CountAmmoVal);
	/* ���������� �������� ������ */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ShowCross(bool ShowPanel);
	/*��������� ������ �������� */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UpdateHealth(int32 HealthVal);
	/*��������� �������� ��� �����������*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UpdateReloadBar(bool ShowBar, float percent);


	/* �������� ��������� � �������� ���������� ��� ������*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void GetArrowComponent(UArrowComponent* &component);


	/* �������� � ���� �������������� � ��������� */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SelectItem(AActor* item);
	/* ������� ��� �������������� */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void GetSelectItem(AActor* &item);
	/* ������� ������� ��� �������������� */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ClearSelectItem();
};
